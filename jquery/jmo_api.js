$( document ).ready(function(){
    var coords = [41.390205, 2.154007]; 
    var zoomLevel = 12
    var map = L.map('map').setView(coords, zoomLevel);
    
    L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
        subdomains: ['a','b','c']
    }).addTo( map );

    $.getJSON(
        "https://api.citybik.es/v2/networks/bicing",
        function (data) {
        var estacions = data.network.stations;
        for ( i in estacions ) 
        {  // MAPA
            L.marker( [data.network.stations[i].latitude, data.network.stations[i].longitude] )
            .bindPopup( '<p style="font-weight:bold;">'+ 'Nombre estacion: '+ data.network.stations[i].name +'</p>' )
            .addTo( map ); 
           //DATOS
            name_estacion = data.network.stations[i].name;
            bicis_disponibles = data.network.stations[i].free_bikes;
            slots_libres = data.network.stations[i].empty_slots;
            latitud = data.network.stations[i].latitude;
            longitud = data.network.stations[i].longitude;
            $('#mitabla tbody').append('<tr><td>' + name_estacion + '</td>' + '<td>' + bicis_disponibles + '</td>' + '<td>' + slots_libres + '</td>' + '<td>' + latitud + '</td>' + '<td>' + longitud + '</td>' + '</tr>')
        }
    });
});