

var datos = ["lunes", "martes", "miércoles"];


function llenaLista() {
    for (i in datos) {
        let elemento = $("<li>").text(datos[i]);
        $("#lista").append(elemento);
    }
}

function llenaTabla() {

    let agenda = [
        { "nombre": "ricard", "telefono": "98798" },
        { "nombre": "joan", "telefono": "987932428" },
        { "nombre": "anna", "telefono": "76767" },
        { "nombre": "pere", "telefono": "98345435798" },
    ];

    for (i in agenda) {
        let fila = $("<tr>");
        let col1 = $("<td>").text(agenda[i].nombre);
        let col2 = $("<td>").text(agenda[i].telefono);
        fila.append(col1).append(col2);

        $("table#mitabla tbody").append(fila);
    }


}




var url = "https://api.citybik.es/v2/networks";

function llenaBikes() {

    $.getJSON(url, procesaDatos);
    console.log("datos solicitados...");
}

function procesaDatos(datos) {
  
    let listaSitios = datos.networks;
    for (indice in listaSitios) {
        let nombre = datos.networks[indice].name;
        let link = "https://api.citybik.es"+datos.networks[indice].href;
        let ciudad = datos.networks[indice].location.city;

        let fila = $("<tr>");
        let col1 = $("<td>").text(nombre);
        let col2 = $("<td>").text(ciudad);
        let enlace = $("<a>").attr({"href": link, "target":"_blank"})
                                .text("Consultar API");
        let col3 = $("<td>").append(enlace);
        fila.append(col1).append(col2).append(col3);
        $("table#bikes tbody").append(fila);
    }


}