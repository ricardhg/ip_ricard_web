import React from "react";
import "./app.css";

function Titulo(props){
  return (<h1 className={props.clase}>{props.texto}</h1>);
}


function Subtitulo(){
  return (<p>lorem ipsum asldfañsld kfñasl kfñalskfd</p>)
}

function Gato(props){
  // http://placekitten.com/100/100
  let url = "http://placekitten.com/"+props.tamanyo+"/"+props.tamanyo;
  return (
    <img src={url} className={props.clase} alt="gato" />
  );
}

function Bola(props){
  let clasesBola = "bola "+props.tipo;
  return <div className={clasesBola}  />
}

function Cosa(){
  return (<>
      <Titulo texto="hola" clase="rojo" />
      <Titulo texto="que tal" clase="verde"  />
      <Gato tamanyo="250" clase="borde" />
      <br />
      <Bola tipo="rojo" ancho="200px" />
      <Bola tipo="rojo" ancho="100px" />
      <Bola tipo="azul" />
      <Subtitulo />
      </>
   );
}

export default Cosa;
